require 'rails_helper'

RSpec.describe ResumeController, type: :controller do

  describe "GET #glenn" do
    it "returns http success" do
      get :glenn
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #duan" do
    it "returns http success" do
      get :duan
      expect(response).to have_http_status(:success)
    end
  end

end
