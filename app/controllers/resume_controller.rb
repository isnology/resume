class ResumeController < ApplicationController
  
  def glenn
    session[:subject] = 'Glenn Marks'
    @files = Dir.entries("./app/views/display_text") - ['.', '..', 'textfile.html.erb']
  end

  def duan
    session[:subject] = 'Sawangduan Sripat'
  end
  
end
