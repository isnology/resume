Rails.application.routes.draw do
  get '/glenn', to: 'resume#glenn'

  get '/duan', to: 'resume#duan'
  
  get '/textfile', to: 'display_text#textfile'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
